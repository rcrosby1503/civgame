﻿using UnityEngine;
using System.Collections.Generic;

public class Node {
    public List<Node> neighbors;
    public int x;
    public int y;
    public float probability;
	public bool isProbable = false;
	public bool start = false;
	public bool exit = false;
	public bool gold = false;
	public bool silver = false;
	public bool bronze = false;

    public Node() {
        neighbors = new List<Node>();
    }

    public float DistanceTo(Node n) {
        return Vector2.Distance(
                new Vector2(x, y),
                new Vector2(n.x, n.y)
            );
    }
}
