﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Items : MonoBehaviour {

	public int tileX;
	public int tileY;
	public TileMap map;

	public List<Node> theMap;
	public List<Node> currentPath = null;

	// Update is called once per frame
	void Update () {
		//Vector3 location = map.TileCoordtoWorldCoord(tileX, tileY);
		transform.position = map.TileCoordtoWorldCoord(tileX, tileY);
	}
}
