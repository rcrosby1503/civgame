﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	private GUIStyle style = new GUIStyle();

	void OnGUI(){
		style.fontSize = 60;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Label (new Rect (0, 0, Screen.width, Screen.height), "Probabalistic Smart Terrain!\nPress Space to start\nPress Esc to close", style);
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Space)){
			SceneManager.LoadScene (1);
		}
		else if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}
	}
}
