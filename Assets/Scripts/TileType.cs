﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TileType {

    public string name;
    public GameObject tileVisualPrefab;

    public float movementCost = 1;

    public bool isWalkable = true;
    public bool startingSquare = false;

    public float probability = 0.0f;



}
