﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Unit : MonoBehaviour {

    public int tileX;
    public int tileY;
    public TileMap map;

    public List<Node> currentPath = null;

	private bool drawline = false;

    int moveSpeed = 2;
	float remainingMovement=2;

    void Update() {
		if(currentPath != null){
			int curNode = 0;
			while (curNode < currentPath.Count - 1) {
				Vector3 start = map.TileCoordtoWorldCoord (currentPath [curNode].x, currentPath [curNode].y) + new Vector3 (0, 0, -0.5f);
				Vector3 end = map.TileCoordtoWorldCoord (currentPath [curNode + 1].x, currentPath [curNode + 1].y) + new Vector3 (0, 0, -0.5f);
				if (drawline)
					Debug.DrawLine (start, end, Color.red);
				curNode++;
			}
		}

		if(Vector3.Distance(transform.position,map.TileCoordtoWorldCoord(tileX,tileY))<0.1f){
			pathing ();
		}
		transform.position = Vector3.Lerp (transform.position, map.TileCoordtoWorldCoord (tileX, tileY), 3f * Time.deltaTime);

		if(Input.GetKeyDown(KeyCode.L)){
			if (drawline)
				drawline = false;
			else
				drawline = true;
		}

		/*
        if(currentPath != null) {
            int currNode = 0;
            while(currNode < currentPath.Count-1) {
                Vector3 start = map.TileCoordtoWorldCoord(currentPath[currNode].x, currentPath[currNode].y) + new Vector3(0,0,-1f);
                Vector3 end = map.TileCoordtoWorldCoord(currentPath[currNode + 1].x, currentPath[currNode + 1].y) + new Vector3(0,0,-1f);
				transform.position = Vector3.Lerp (transform.position, end, 0.5f * Time.deltaTime);
				if(drawline)
                	Debug.DrawLine(start, end, Color.red);
                currNode++;
            }
        }
		if(Input.GetKeyDown(KeyCode.J)){
			if (drawline)
				drawline = false;
			else
				drawline = true;
		}*/
    }

	void OnCollisionEnter(Collision col){
		Debug.Log ("guard hit: " + col.gameObject.name);
		if(col.gameObject.name == "Player"){
			Destroy (col.gameObject);
			//game over screen.
			SceneManager.LoadScene(2);
		}
	}

	void pathing(){
		if (currentPath == null)
			return;
		if (remainingMovement <= 0) {
			map.adjustProbability ();
			nextTurn ();
		}
		
		transform.position = map.TileCoordtoWorldCoord (tileX, tileY);
		remainingMovement -= map.CostToEnterTile (currentPath [0].x, currentPath [0].y, currentPath [1].x, currentPath [1].y);

		tileX = currentPath [1].x;
		tileY = currentPath [1].y;

		currentPath.RemoveAt (0);

		if(currentPath.Count == 1){
			map.numProbTiles--;
			map.ResetNodeProbability(tileX, tileY);
			currentPath = null;
			//map.CalcGuardTarget();
			map.SimulateDistance();
			Debug.Log("New Guard X: " + map.getGuardX());
			Debug.Log("New Guard Y: " + map.getGuardY());
			map.GeneratePathTo(map.getGuardX(), map.getGuardY());
			nextTurn ();
		}
	}

	public void nextTurn(){
		while(currentPath != null && remainingMovement > 0){
			pathing ();
		}
		remainingMovement = moveSpeed;
	}

	/*
    public void MoveNextTile() {
        float remainingMovement = moveSpeed;
        while (remainingMovement > 0) {
            if (currentPath == null) return;
            //Get cost from current tile
            remainingMovement -= map.CostToEnterTile(currentPath[0].x, currentPath[0].y, currentPath[1].x, currentPath[1].y);
            //grab new first node and move
            tileX = currentPath[1].x;
            tileY = currentPath[1].y;
			Vector3 curPos = new Vector3 (currentPath[0].x, currentPath[0].y, 0);
			Vector3 destPos = new Vector3 (tileX, tileY, 0);
			//transform.position = Vector3.Lerp (curPos, map.TileCoordtoWorldCoord (tileX, tileY), 0.5f * Time.deltaTime);
            //transform.position = map.TileCoordtoWorldCoord(tileX, tileY);
            //remove old current/first node
            currentPath.RemoveAt(0);

            if (currentPath.Count == 1) {
                //we only have one tile left in path, we are standing on it, clear pathfinding info

                map.ResetNodeProbability(tileX, tileY);
                //map.numProbTiles--;
                currentPath = null;
                map.CalcGuardTarget();
                Debug.Log("New Guard X: " + map.getGuardX());
                Debug.Log("New Guard Y: " + map.getGuardY());
                map.GeneratePathTo(map.getGuardX(), map.getGuardY());
            }

        }
    }
    */

}
