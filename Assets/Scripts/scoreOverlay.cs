﻿using UnityEngine;
using System.Collections;

public class scoreOverlay : MonoBehaviour {
	public Player player;
	public int exit;

	private GUIStyle style = new GUIStyle();

	void OnGUI(){
		style.fontSize = 20;
		style.normal.textColor = Color.white;
		GUI.Label (new Rect (50, 50, Screen.width, Screen.height), "Score: " + player.playerPoints, style);
		if (player.atExit)
			exit = 1;
		else
			exit = 0;
		Debug.Log("at exit = " + exit);
		if(player.atExit && player.playerPoints < 500){
			Debug.Log ("hello!");
			GUI.Label (new Rect ((Screen.width/2) - 100, Screen.height/2, Screen.width, Screen.height), "You need at least 500 points to progress!", style);
		}
	}
		
}
