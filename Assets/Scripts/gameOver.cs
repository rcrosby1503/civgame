﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class gameOver : MonoBehaviour {

	void OnGUI(){
		GUI.Label (new Rect (Screen.width/2, Screen.height/2, Screen.width, Screen.height), "Game Over");
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Space)){
			SceneManager.LoadScene (0);
		}
	}
}
