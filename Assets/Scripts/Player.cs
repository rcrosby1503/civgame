﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    public int tileX;
    public int tileY;
    public TileMap map;

    public List<Node> theMap;
    public List<Node> currentPath = null;

	private int goldPoints = 1000;
	private int silverPoints = 500;
	private int bronzePoints = 350;
	public int playerPoints = 0;
	public bool atExit = false;

    // Update is called once per frame
    void Update () {
        //Vector3 location = map.TileCoordtoWorldCoord(tileX, tileY);
		if(map.gameStart){
			transform.position = map.TileCoordtoWorldCoord (tileX, tileY);
			map.gameStart = false;
		}
        //transform.position = map.TileCoordtoWorldCoord(tileX, tileY);
		if(Input.GetKeyDown(KeyCode.W)){
			if(tileY < map.getMapSizeY() && map.UnitCanEnter(tileX,tileY+1)){
				tileY++;
			}
		}
		if(Input.GetKeyDown(KeyCode.S)){
			if(tileY > 0 && map.UnitCanEnter(tileX, tileY-1)){
				tileY--;
			}
		}
		if(Input.GetKeyDown(KeyCode.A)){
			if(tileX > 0 && map.UnitCanEnter(tileX-1, tileY)){
				tileX--;
			}
		}
		if(Input.GetKeyDown(KeyCode.D)){
			if(tileX < map.getMapSizeX() && map.UnitCanEnter(tileX+1,tileY)){
				tileX++;
			}
		}
		transform.position = Vector3.Lerp (transform.position, map.TileCoordtoWorldCoord (tileX, tileY), 3f * Time.deltaTime);
		atExit = map.checkExit (tileX, tileY);
    }

	void OnCollisionEnter(Collision col){
		Debug.Log ("Hello! I hit something! " + col.gameObject.name);
		if(col.gameObject.name == "itemGold"){
			Debug.Log ("Hello! Touching gold!");
			Destroy (col.gameObject);
			playerPoints += goldPoints;
		}
		if(col.gameObject.name == "itemSilver"){
			Debug.Log ("Hello! Touching Silver!");
			Destroy(col.gameObject);
			playerPoints += silverPoints;
		}
		if(col.gameObject.name == "itemBronze"){
			Debug.Log ("Hello! Touching Bronze");
			Destroy (col.gameObject);
			playerPoints += bronzePoints;
		}
	}

	public void setExit(bool exit){
		atExit = exit;
	}
}
